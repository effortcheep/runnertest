//
//  SceneDelegate.h
//  RunnerTest
//
//  Created by Effort on 2019/10/23.
//  Copyright © 2019 Effort. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SceneDelegate : UIResponder <UIWindowSceneDelegate>

@property (strong, nonatomic) UIWindow * window;

@end

